#include <iostream>
#include <conio.h>

using namespace std;

int main() {
	int k = 0;
	const int n = 5;
	int a[n];
	int b[n];
	for (int i = 0; i<n; i++){
		cout << "A[" << i + 1 << "]=";
		cin >> a[i];
	}
	for (int i = 0; i<n; i++){
		cout << "B[" << i + 1 << "]=";
		cin >> b[i];
	}
	for (int i = 0; i<n; i++)
	for (int j = 0; j<n; j++)
	if (a[j] > a[j + 1]) {
		swap(a[j], a[j + 1]);
		swap(b[j], b[j + 1]);
	}
	for (int i = 0; i<n; i++){
		for (int j = i + 1; j<n; j++){
			if (b[i]>b[j])
				k++;
		}
	}
	cout << k;
	_getch();
	return 0;
}
